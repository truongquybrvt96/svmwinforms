﻿using HtmlAgilityPack;
using SVM.Commons;
using SVM.Core.Interfaces;
using SVM.Forms;
using SVM.Persistence;
using System;
using System.Windows.Forms;

namespace SVM
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            HtmlWeb htmlWeb = new HtmlWeb();
            IHelpers helpers = new Helpers();

            IWebDriverRepository webDriverRepository = new ChromeDriverRepository();
            IWebDriverRepository webDriverRepositoryHeadless = new ChromeDriverRepository();

            I48IdolRepository _48IdolRepository = new _48IdolRepository(webDriverRepository);
            ILatestUpdatesRepository latestUpdatesRepository = new LatestUpdates48Idol(webDriverRepository);

            //Video Downloader Form
            frmVideoDownloader frmVideoDownloader = new frmVideoDownloader(_48IdolRepository);
            frmVideoDownloader.TopLevel = false;
            this.tabMain.TabPages[0].Controls.Add(frmVideoDownloader);
            frmVideoDownloader.Dock = DockStyle.Fill;
            frmVideoDownloader.Show();

            //Latest Updates Form
            frmLatestUpdates frmLatestUpdates = new frmLatestUpdates(latestUpdatesRepository, helpers);
            frmLatestUpdates.TopLevel = false;
            this.tabMain.TabPages[1].Controls.Add(frmLatestUpdates);
            frmLatestUpdates.Dock = DockStyle.Fill;
            frmLatestUpdates.Show();
        }
    }
}
