﻿using HtmlAgilityPack;
using SVM.Core.Interfaces;
using SVM.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVM.Forms
{
    public partial class frmVideoDownloader : Form
    {
        private string _videoUrl;

        private readonly I48IdolRepository  i48IdolRepository;

        public frmVideoDownloader(I48IdolRepository i48IdolRepository)
        {
            InitializeComponent();
            this.i48IdolRepository = i48IdolRepository;
        }

        private void frmVideoDownloader_Load(object sender, EventArgs e)
        {
            btnOpenFileUrl.Enabled = false;
        }

        private async void btnGet_Click(object sender, EventArgs e)
        {
            _videoUrl = null;
            btnGet.Enabled = false;
            btnOpenFileUrl.Enabled = false;
            try
            {
                var resultInfo = await Task.Run(() => i48IdolRepository.GetVideoData(txtVideoUrl.Text));
                if (resultInfo == null)
                    MessageBox.Show("Failed!");
                else
                {
                    dgvDownloadInfo.Rows.Clear();
                    PopulateDGV(resultInfo, dgvDownloadInfo);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Lỗi rồi đó má!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            btnGet.Enabled = true;
        }

        private void btnOpenFileUrl_Click(object sender, EventArgs e)
        {
            Process.Start(_videoUrl);
        }

        private void PopulateDGV(_48IdolVideo _48IdolVideo, DataGridView dataGridView)
        {
            DataGridViewRow row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "Title";
            row.Cells[1].Value = _48IdolVideo.Title;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "Time";
            row.Cells[1].Value = _48IdolVideo.Time;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "Views in 7 days";
            row.Cells[1].Value = _48IdolVideo.Views;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "Get time";
            row.Cells[1].Value = _48IdolVideo.GetTime;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "UpToBox Raw";
            row.Cells[1].Value = _48IdolVideo.UpToBoxRawUrl;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "UpToBox Premium";
            row.Cells[1].Value = _48IdolVideo.UpToBoxPremiumUrl;
            dataGridView.Rows.Add(row);

            row = (DataGridViewRow)dataGridView.Rows[0].Clone();
            row.Cells[0].Value = "Stream File";
            row.Cells[1].Value = _48IdolVideo.VideoStreamFileUrl;
            dataGridView.Rows.Add(row);

            dataGridView.Refresh();
        }
    }
}
