﻿namespace SVM.Forms
{
    partial class frmLatestUpdates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLatestUpdates));
            this.dgvVideos = new System.Windows.Forms.DataGridView();
            this.btnUpdateNow = new System.Windows.Forms.Button();
            this.btnToggleAutoUpdate = new System.Windows.Forms.Button();
            this.nudMinuteCheck = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnToggleAutoDownload = new System.Windows.Forms.Button();
            this.newColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColGetTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColFileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColDownloadProgress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newColDownload = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGetTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVideoURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOpen = new System.Windows.Forms.DataGridViewButtonColumn();
            this.grbSettings = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAutoSaveFolder = new System.Windows.Forms.TextBox();
            this.btnAutoSaveFolder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVideos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuteCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.grbSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvVideos
            // 
            this.dgvVideos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVideos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colImage,
            this.colTitle,
            this.colTime,
            this.colGetTime,
            this.colVideoURL,
            this.colOpen});
            this.dgvVideos.Location = new System.Drawing.Point(12, 349);
            this.dgvVideos.Name = "dgvVideos";
            this.dgvVideos.RowTemplate.Height = 30;
            this.dgvVideos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVideos.Size = new System.Drawing.Size(1320, 310);
            this.dgvVideos.TabIndex = 0;
            // 
            // btnUpdateNow
            // 
            this.btnUpdateNow.AutoSize = true;
            this.btnUpdateNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateNow.Location = new System.Drawing.Point(13, 13);
            this.btnUpdateNow.Name = "btnUpdateNow";
            this.btnUpdateNow.Size = new System.Drawing.Size(140, 27);
            this.btnUpdateNow.TabIndex = 1;
            this.btnUpdateNow.Text = "Update Now";
            this.btnUpdateNow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateNow.UseVisualStyleBackColor = true;
            this.btnUpdateNow.Click += new System.EventHandler(this.btnUpdateNow_Click);
            // 
            // btnToggleAutoUpdate
            // 
            this.btnToggleAutoUpdate.AutoSize = true;
            this.btnToggleAutoUpdate.BackColor = System.Drawing.Color.IndianRed;
            this.btnToggleAutoUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToggleAutoUpdate.Location = new System.Drawing.Point(13, 46);
            this.btnToggleAutoUpdate.Name = "btnToggleAutoUpdate";
            this.btnToggleAutoUpdate.Size = new System.Drawing.Size(140, 27);
            this.btnToggleAutoUpdate.TabIndex = 2;
            this.btnToggleAutoUpdate.Text = "Auto Update: Off";
            this.btnToggleAutoUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToggleAutoUpdate.UseVisualStyleBackColor = false;
            this.btnToggleAutoUpdate.Click += new System.EventHandler(this.btnToggleAutoUpdate_Click);
            // 
            // nudMinuteCheck
            // 
            this.nudMinuteCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudMinuteCheck.Location = new System.Drawing.Point(109, 23);
            this.nudMinuteCheck.Name = "nudMinuteCheck";
            this.nudMinuteCheck.Size = new System.Drawing.Size(94, 23);
            this.nudMinuteCheck.TabIndex = 3;
            this.nudMinuteCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudMinuteCheck.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(209, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "minute(s)";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.newColId,
            this.newColTitle,
            this.newColTime,
            this.newColGetTime,
            this.newColFileSize,
            this.newColDownloadProgress,
            this.newColDownload});
            this.dataGridView1.Location = new System.Drawing.Point(13, 119);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1319, 211);
            this.dataGridView1.TabIndex = 5;
            // 
            // btnToggleAutoDownload
            // 
            this.btnToggleAutoDownload.AutoSize = true;
            this.btnToggleAutoDownload.BackColor = System.Drawing.Color.IndianRed;
            this.btnToggleAutoDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToggleAutoDownload.Location = new System.Drawing.Point(13, 79);
            this.btnToggleAutoDownload.Name = "btnToggleAutoDownload";
            this.btnToggleAutoDownload.Size = new System.Drawing.Size(140, 27);
            this.btnToggleAutoDownload.TabIndex = 6;
            this.btnToggleAutoDownload.Text = "Auto Download: Off";
            this.btnToggleAutoDownload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnToggleAutoDownload.UseVisualStyleBackColor = false;
            // 
            // newColId
            // 
            this.newColId.HeaderText = "ID";
            this.newColId.Name = "newColId";
            this.newColId.Width = 30;
            // 
            // newColTitle
            // 
            this.newColTitle.HeaderText = "Title";
            this.newColTitle.Name = "newColTitle";
            this.newColTitle.Width = 200;
            // 
            // newColTime
            // 
            this.newColTime.HeaderText = "Time";
            this.newColTime.Name = "newColTime";
            // 
            // newColGetTime
            // 
            this.newColGetTime.HeaderText = "Get Time";
            this.newColGetTime.Name = "newColGetTime";
            // 
            // newColFileSize
            // 
            this.newColFileSize.HeaderText = "File Size";
            this.newColFileSize.Name = "newColFileSize";
            // 
            // newColDownloadProgress
            // 
            this.newColDownloadProgress.HeaderText = "Download Progress";
            this.newColDownloadProgress.Name = "newColDownloadProgress";
            this.newColDownloadProgress.Width = 150;
            // 
            // newColDownload
            // 
            this.newColDownload.HeaderText = "Download";
            this.newColDownload.Name = "newColDownload";
            this.newColDownload.Text = "Save";
            // 
            // colId
            // 
            this.colId.HeaderText = "ID";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Width = 35;
            // 
            // colImage
            // 
            this.colImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colImage.HeaderText = "Image";
            this.colImage.Image = ((System.Drawing.Image)(resources.GetObject("colImage.Image")));
            this.colImage.Name = "colImage";
            this.colImage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colImage.Width = 130;
            // 
            // colTitle
            // 
            this.colTitle.HeaderText = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.colTitle.Width = 200;
            // 
            // colTime
            // 
            this.colTime.HeaderText = "Time";
            this.colTime.Name = "colTime";
            // 
            // colGetTime
            // 
            this.colGetTime.HeaderText = "Get Time";
            this.colGetTime.Name = "colGetTime";
            // 
            // colVideoURL
            // 
            this.colVideoURL.HeaderText = "Video URL";
            this.colVideoURL.Name = "colVideoURL";
            // 
            // colOpen
            // 
            this.colOpen.HeaderText = "Open";
            this.colOpen.Name = "colOpen";
            // 
            // grbSettings
            // 
            this.grbSettings.Controls.Add(this.btnAutoSaveFolder);
            this.grbSettings.Controls.Add(this.txtAutoSaveFolder);
            this.grbSettings.Controls.Add(this.label3);
            this.grbSettings.Controls.Add(this.label2);
            this.grbSettings.Controls.Add(this.nudMinuteCheck);
            this.grbSettings.Controls.Add(this.label1);
            this.grbSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbSettings.Location = new System.Drawing.Point(428, 13);
            this.grbSettings.Name = "grbSettings";
            this.grbSettings.Size = new System.Drawing.Size(904, 93);
            this.grbSettings.TabIndex = 7;
            this.grbSettings.TabStop = false;
            this.grbSettings.Text = "Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Update every:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Save folder:";
            // 
            // txtAutoSaveFolder
            // 
            this.txtAutoSaveFolder.Location = new System.Drawing.Point(109, 53);
            this.txtAutoSaveFolder.Name = "txtAutoSaveFolder";
            this.txtAutoSaveFolder.Size = new System.Drawing.Size(384, 23);
            this.txtAutoSaveFolder.TabIndex = 7;
            this.txtAutoSaveFolder.Leave += new System.EventHandler(this.txtAutoSaveFolder_Leave);
            // 
            // btnAutoSaveFolder
            // 
            this.btnAutoSaveFolder.Location = new System.Drawing.Point(500, 53);
            this.btnAutoSaveFolder.Name = "btnAutoSaveFolder";
            this.btnAutoSaveFolder.Size = new System.Drawing.Size(75, 23);
            this.btnAutoSaveFolder.TabIndex = 8;
            this.btnAutoSaveFolder.Text = "Select...";
            this.btnAutoSaveFolder.UseVisualStyleBackColor = true;
            this.btnAutoSaveFolder.Click += new System.EventHandler(this.btnAutoSaveFolder_Click);
            // 
            // frmLatestUpdates
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1344, 671);
            this.Controls.Add(this.grbSettings);
            this.Controls.Add(this.btnToggleAutoDownload);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnToggleAutoUpdate);
            this.Controls.Add(this.btnUpdateNow);
            this.Controls.Add(this.dgvVideos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLatestUpdates";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Latest Updates";
            this.Load += new System.EventHandler(this.frmLatestUpdates_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVideos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuteCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.grbSettings.ResumeLayout(false);
            this.grbSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVideos;
        private System.Windows.Forms.Button btnUpdateNow;
        private System.Windows.Forms.Button btnToggleAutoUpdate;
        private System.Windows.Forms.NumericUpDown nudMinuteCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewImageColumn colImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGetTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVideoURL;
        private System.Windows.Forms.DataGridViewButtonColumn colOpen;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColId;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColGetTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColFileSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn newColDownloadProgress;
        private System.Windows.Forms.DataGridViewButtonColumn newColDownload;
        private System.Windows.Forms.Button btnToggleAutoDownload;
        private System.Windows.Forms.GroupBox grbSettings;
        private System.Windows.Forms.Button btnAutoSaveFolder;
        private System.Windows.Forms.TextBox txtAutoSaveFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}