﻿using SVM.Commons;
using SVM.Core.Interfaces;
using SVM.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SVM.Forms
{
    public partial class frmLatestUpdates : Form
    {
        private readonly ILatestUpdatesRepository latestUpdatesRepository;
        private readonly IHelpers helpers;
        private Task updateTask;
        private List<Video> baseVideoList;

        public frmLatestUpdates(ILatestUpdatesRepository latestUpdatesRepository, IHelpers helpers)
        {
            InitializeComponent();
            this.latestUpdatesRepository = latestUpdatesRepository;
            this.helpers = helpers;
        }

        private void frmLatestUpdates_Load(object sender, EventArgs e)
        {
        }

        private async void btnUpdateNow_Click(object sender, EventArgs e)
        {
            dgvVideos.Rows.Clear();

            btnUpdateNow.Enabled = false;
            updateTask = Task.Run(() => UpdateVideos(true));
            await updateTask;
            btnUpdateNow.Enabled = true;
        }

        private void UpdateVideos(bool refresherBaseVideos = false)
        {
            var videos = latestUpdatesRepository.GetUpdates();

            foreach (var video in videos)
            {
                var newRow = (DataGridViewRow)dgvVideos.Rows[0].Clone();
                newRow.Cells[0].Value = video.Id;
                newRow.Cells[1].Value = helpers.GetImage(video.ImageUrl);
                newRow.Cells[2].Value = video.Title;
                newRow.Cells[3].Value = video.Time;
                newRow.Cells[4].Value = video.GetTime;
                newRow.Cells[5].Value = video.VideoUrl;

                dgvVideos.Invoke(new MethodInvoker(() => dgvVideos.Rows.Add(newRow)));
            }
            if(refresherBaseVideos)
            {
                baseVideoList = new List<Video>();
                baseVideoList.AddRange(videos);
            }
        }

        private void btnAutoSaveFolder_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.ValidateNames = false;
                openFileDialog.CheckFileExists = false;
                openFileDialog.CheckPathExists = true;
                openFileDialog.FileName = "Folder Selection.";

                if(openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtAutoSaveFolder.Text = Path.GetDirectoryName(openFileDialog.FileName);
                }
            }
        }

        private void txtAutoSaveFolder_Leave(object sender, EventArgs e)
        {
            if(!Directory.Exists(txtAutoSaveFolder.Text))
            {
                MessageBox.Show("The folder does not exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnToggleAutoUpdate_Click(object sender, EventArgs e)
        {
            btnToggleAutoUpdate.Text = "Auto Update: On";
            btnToggleAutoUpdate.BackColor = Color.Green;
        }

        private void AutoUpdate()
        {
            if(updateTask.Status != TaskStatus.Running)
            {
                var timer = new TimerManager(() =>
                {

                },
                Convert.ToInt32(nudMinuteCheck.Value) * 60 * 1000);
            }
            
        }
    }
}
