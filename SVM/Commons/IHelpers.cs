﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Commons
{
    public interface IHelpers
    {
        Bitmap GetImage(string url);
    }
}
