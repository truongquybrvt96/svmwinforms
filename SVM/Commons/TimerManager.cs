﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SVM.Commons
{
    public class TimerManager : IDisposable
    {
        private readonly Action action;
        private AutoResetEvent autoResetEvent;
        private Timer timer;

        public TimerManager(Action action, int time = 300000)
        {
            this.action = action;
            autoResetEvent = new AutoResetEvent(false);
            timer = new Timer(Execute, autoResetEvent, 1000, time);
        }

        public void Dispose()
        {
            timer.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Execute(object stateInfo)
        {
            action();
        }
    }
}
