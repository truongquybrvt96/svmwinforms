﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Commons
{
    public class Helpers : IHelpers
    {
        public Bitmap GetImage(string url)
        {
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = "GET";
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                Bitmap bmp = new Bitmap(myResponse.GetResponseStream());
                myResponse.Close();

                return bmp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
