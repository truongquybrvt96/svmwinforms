﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using SVM.Core.Interfaces;
using SVM.Core.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Persistence
{
    public class _48IdolRepository : I48IdolRepository
    {
        private readonly IWebDriverRepository webDriverRepository;
        private HtmlWeb htmlWeb;

        public _48IdolRepository(IWebDriverRepository webDriverRepository)
        {
            htmlWeb = new HtmlWeb();
            this.webDriverRepository = webDriverRepository;
        }

        public void Dispose()
        {
            htmlWeb = null;
            webDriverRepository.Dispose();

            GC.SuppressFinalize(this);
        }

        public _48IdolVideo GetVideoData(string url)
        {
            try
            {
                if (!CheckValid48idolVideoUrl(url))
                    return null;

                //Embed
                var rawDoc = htmlWeb.Load(url);
                var iframe = rawDoc.DocumentNode.SelectSingleNode("//div[@id='div-desktop']//div[@id='selectorElement']//div/iframe[1]");
                var embedUrl = iframe.Attributes["src"].Value;

                //VideoStreamFileUrl
                var videoStreamFileUrl = GetVideoStreamFileUrl(embedUrl);

                //Title
                var titleNode = rawDoc.DocumentNode.SelectSingleNode("//div[@class='post-title']/h2[1]");
                if (titleNode == null)
                    return null;
                var title = titleNode.InnerText;

                //Time
                var timeNode = rawDoc.DocumentNode.SelectSingleNode("//div[@class='post-title']/p[1]/span[1]");
                if (timeNode == null)
                    return null;
                DateTime time;
                var timeVaid = DateTime.TryParseExact(timeNode.InnerText, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out time);
                if (!timeVaid)
                    return null;

                //Views
                var viewsNode = rawDoc.DocumentNode.SelectSingleNode("//div[@class='post-title']/p[1]/span[2]");
                if (viewsNode == null)
                    return null;
                var viewsNodeText = viewsNode.InnerHtml;
                var viewsText = viewsNodeText.Substring(viewsNodeText.LastIndexOf("</i>") + 4, viewsNodeText.LastIndexOf(" views") - viewsNodeText.LastIndexOf("</i>") - 4);
                int views;
                var viewsValid = int.TryParse(viewsText, out views);
                if (!viewsValid)
                    return null;

                //UpToBoxRawUrl
                var upToBoxRawUrlNode = rawDoc.DocumentNode.SelectSingleNode("//button[@id='ddb']");
                if (upToBoxRawUrlNode == null)
                    return null;
                var onclickValue = upToBoxRawUrlNode.Attributes["onclick"].Value;
                var upToBoxRawUrl = onclickValue.Substring(onclickValue.IndexOf("http"), onclickValue.LastIndexOf("',") - onclickValue.IndexOf("http"));

                //UpToBoxPremiumUrl
                var upToBoxPremiumUrl = GetPremiumUpToBox(upToBoxRawUrl);


                _48IdolVideo result = new _48IdolVideo
                {
                    GetTime = DateTime.Now,
                    Time = time,
                    Title = title,
                    UpToBoxPremiumUrl = upToBoxPremiumUrl,
                    UpToBoxRawUrl = upToBoxRawUrl,
                    VideoStreamFileUrl = videoStreamFileUrl,
                    Views = views
                };

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            

        }

        public string GetVideoStreamFileUrl(string embedUrl)
        {
            if (!Uri.IsWellFormedUriString(embedUrl, UriKind.Absolute))
                return null;

            webDriverRepository.Headless = true;
            webDriverRepository.Start();

            var doc = webDriverRepository.GetDocument(embedUrl);
            var videoNodes = doc.DocumentNode.SelectNodes("//video[@data-html5-video]");

            if (videoNodes == null || videoNodes.Count == 0)
                return String.Empty;

            string videoUrl = String.Empty;
            foreach (var videoNode in videoNodes)
            {
                videoUrl = videoNode.GetAttributeValue("src", String.Empty);
            }
            webDriverRepository.Dispose();
            return videoUrl;
        }




        private bool CheckValid48idolVideoUrl(string url)
        {
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return false;
            var index = url.LastIndexOf("/") + 1;
            var length = url.Length - url.LastIndexOf("/") - 1;
            string videoNumber = url.Substring(index, length);

            if (String.IsNullOrEmpty(videoNumber) || String.IsNullOrWhiteSpace(videoNumber))
                return false;

            if (!videoNumber.All(char.IsDigit))
                return false;

            return true;
        }

        public string GetPremiumUpToBox(string url)
        {
            webDriverRepository.Headless = false;
            webDriverRepository.Start();
            ChromeDriver driver = (ChromeDriver)webDriverRepository.Driver;
            driver.Navigate().GoToUrl(url);
            var expectedEl = new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementExists(By.XPath("//div[@id='footer']")));

            var logEl = driver.FindElement(By.XPath("//a[contains(text(),'Login')]"));
            if (logEl != null)
            {
                logEl.Click();
                var logButton = new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementExists(By.XPath("//button[contains(@class,'login')]")));
                if (logButton == null)
                    return null;
                var userInput = driver.FindElementByXPath("//input[@placeholder='Username']");
                var pwdInput = driver.FindElementByXPath("//input[@placeholder='Password']");
                if(userInput != null && pwdInput != null)
                {
                    userInput.SendKeys("truongquybrvt96");
                    pwdInput.SendKeys("123@123aA");
                    logButton.Submit();

                    var premiumButton = new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementExists(By.XPath("//a[@class='big-button-green-flat mt-4 mb-4']")));
                    var premiumUrl = premiumButton.GetAttribute("href");
                    webDriverRepository.Dispose();
                    return premiumUrl;
                }
            }

            var premiumButton2 = new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(ExpectedConditions.ElementExists(By.XPath("//a[@class='big-button-green-flat mt-4 mb-4']")));
            var premiumUrl2 = premiumButton2.GetAttribute("href");
            webDriverRepository.Dispose();
            return premiumUrl2;

        }
    }
}
