﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using SVM.Core.Interfaces;
using SVM.Core.Models;
using SVM.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Persistence
{
    public class LatestUpdates48Idol : ILatestUpdatesRepository, IDisposable
    {
        private readonly string _siteUrl = Resources._48idolVideos;
        private readonly IWebDriverRepository webDriverRepository;

        public LatestUpdates48Idol(IWebDriverRepository webDriverRepository)
        {
            this.webDriverRepository = webDriverRepository;
        }

        public void Dispose()
        {
            if (webDriverRepository != null)
                webDriverRepository.Dispose();
            GC.SuppressFinalize(this);
        }

        public List<Video> GetUpdates()
        {
            var videos = new List<Video>();
            HtmlWeb htmlWeb = new HtmlWeb();
            int id = 1;

            var doc = htmlWeb.Load(_siteUrl);
            var videoBlock = doc.DocumentNode.SelectSingleNode("//div[@class='tabs-panel is-active' and @id='new-all']/div[contains(@class,'list-group')]");

            //Video divs
            var videoDivs = videoBlock.SelectNodes("div[contains(@class,'item') and contains(@class,'list')]").ToList();

            foreach (var video in videoDivs)
            {

                //Image
                var imgNode = video.SelectSingleNode("div[contains(@class,'post thumb-border')]/div[contains(@class,'post-thumb')]/img[contains(@class,'lazyload')]");
                if (imgNode == null)
                    continue;

                var imageSrc = imgNode.Attributes["data-original"].Value;

                //Title
                var desNode = video.SelectSingleNode("div[contains(@class,'post thumb-border')]/div[contains(@class,'post-des')]/h6[1]/a[1]");
                var title = desNode.InnerText;
                var videoUrl = desNode.Attributes["href"].Value;

                //Time
                var timeNode = video.SelectSingleNode("div[contains(@class,'post thumb-border')]/div[contains(@class,'post-des')]/div[contains(@class,'post-stats')]/p[1]/span[1]");
                var timeText = timeNode.InnerText.Trim();

                //Pack
                var vid = new Video
                {
                    Id = id,
                    Time = DateTime.ParseExact(timeText, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None),
                    GetTime = DateTime.Now,
                    ImageUrl = imageSrc,
                    Title = title,
                    VideoUrl = videoUrl
                };

                videos.Add(vid);
                id++;
            }

            return videos;
        }
    }
}
