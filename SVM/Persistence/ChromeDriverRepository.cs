﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SVM.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SVM.Persistence
{
    public class ChromeDriverRepository : IWebDriverRepository, IDisposable
    {
        private ChromeDriver driver;
        private ChromeDriverService chromeService;
        private ChromeOptions chromeOptions;
        private readonly ChromeDriverService services;
        private readonly ChromeOptions options;

        public IWebDriver Driver
        {
            get { return driver; }
        }

        public bool Headless { get; set; } = false;


        public ChromeDriverRepository(ChromeDriverService services = null, ChromeOptions options = null)
        {
            this.services = services;
            this.options = options;
        }

        public void Start()
        {
            //Service. Hide cmd
            if (services == null)
            {
                chromeService = ChromeDriverService.CreateDefaultService();
                chromeService.HideCommandPromptWindow = true;
            }

            //Option. Hide browser
            if (options == null)
            {
                chromeOptions = new ChromeOptions();
                if (Headless)
                    chromeOptions.AddArgument("headless");
            }

            driver = new ChromeDriver(chromeService, chromeOptions);
        }


        public HtmlDocument GetDocument(string url)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
                var sourceString = driver.PageSource;

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(sourceString);
                return doc;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public HtmlDocument GetDocument(string url, TimeSpan timeOut, By by)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
                WebDriverWait wait = new WebDriverWait(driver, timeOut);
                var videoSection = wait.Until(ExpectedConditions.ElementExists(by));


                if (videoSection != null)
                {
                    var sourceString = videoSection.GetAttribute("innerHTML");
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(sourceString);
                    return doc;
                }
                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                driver.Dispose();
            }

        }

        public void Dispose()
        {
            if (driver != null)
                driver.Quit();
            GC.SuppressFinalize(this);
        }
    }
}
