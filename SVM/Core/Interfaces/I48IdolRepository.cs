﻿using SVM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Core.Interfaces
{
    public interface I48IdolRepository : IDisposable
    {
        _48IdolVideo GetVideoData(string url);
        string GetVideoStreamFileUrl(string embedUrl);
        string GetPremiumUpToBox(string url);
    }
}
