﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Core.Interfaces
{
    public interface IWebDriverRepository : IDisposable
    {
        bool Headless { get; set; }
        IWebDriver Driver { get; }
        HtmlDocument GetDocument(string url);
        void Start();

        /// <summary>
        /// Get element html document until when it is found.
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="timeOut">Timeout of waiting.</param>
        /// <param name="by">Find element by.</param>
        /// <returns></returns>
        HtmlDocument GetDocument(string url, TimeSpan timeOut, By by);
    }
}
