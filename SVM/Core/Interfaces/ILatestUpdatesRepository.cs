﻿using SVM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Core.Interfaces
{
    public interface ILatestUpdatesRepository
    {
        List<Video> GetUpdates();
    }
}
