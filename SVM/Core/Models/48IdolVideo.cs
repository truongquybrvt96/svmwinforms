﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Core.Models
{
    public class _48IdolVideo
    {
        public string Title { get; set; }
        public DateTime Time { get; set; }
        public int Views { get; set; }
        public string UpToBoxRawUrl { get; set; }
        public string UpToBoxPremiumUrl { get; set; }
        public DateTime GetTime { get; set; }
        public string VideoStreamFileUrl { get; set; }
    }
}
