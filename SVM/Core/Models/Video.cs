﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Core.Models
{
    public class Video
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public DateTime Time { get; set; }
        public DateTime GetTime { get; set; }
        public string VideoUrl { get; set; }
        public string VideoFileUrl { get; set; }
    }
}
